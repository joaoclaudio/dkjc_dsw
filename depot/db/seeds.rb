Product.delete_all
Product.create!(title: 'O Programador Apaixonado',
	description:
		%{<p>
			Livro que dá dicas sobre como avançar na profissão de programador
			e alcançar seus objetivos profissionais.
		</p>},
	image_url: 'programador-apaixonado.png',
	price: 49.95)

Product.create!(title: 'Ruby',
	description:
		%{<p>
			Ruby é uma linguagem de script moderna e orientada à objetos,
			versátil e dinâmica.
		</p>},
	image_url: 'ruby.png',
	price: 49.95)

Product.create!(title: 'Node.js',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'nodejs.png',
	price: 49.95)

Product.create!(title: 'Desenvolvimento de Jogos para iOS',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'jogos-ios.png',
	price: 49.95)

Product.create!(title: 'Desenvolvimento de Jogos para Android',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'jogos-android.png',
	price: 49.95)

Product.create!(title: 'Web Design Responsivo',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'design-responsivo.png',
	price: 49.95)

Product.create!(title: 'HTML5 e CSS3',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'html5-css3.png',
	price: 49.95)

Product.create!(title: 'Lógica de Programação',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'logica-programacao.png',
	price: 49.95)

Product.create!(title: 'A Web Mobile',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'web-mobile.png',
	price: 49.95)

Product.create!(title: 'Cucumber e RSpec',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'cucumber-rspec.png',
	price: 49.95)

Product.create!(title: 'Agile Web Development with Rails 4',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'agile-dev.png',
	price: 49.95)

Product.create!(title: 'Crafting Rails 4 Applications',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'rails4-apps.png',
	price: 49.95)

Product.create!(title: 'Rest',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'rest.png',
	price: 49.95)

Product.create!(title: 'Design Patterns com Java',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'design-patterns.png',
	price: 49.95)

Product.create!(title: 'iOS',
	description:
		%{<p>
			Node.js é um framework javascript que pretende facilitar a vida
			do desenvolvedor web.
		</p>},
	image_url: 'ios.png',
	price: 49.95)
