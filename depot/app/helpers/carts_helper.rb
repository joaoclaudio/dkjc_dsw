module CartsHelper
	def cart_info(cart)
		"(#{cart.total_quantity} - #{number_to_currency(cart.total_price)})"
	end
end
