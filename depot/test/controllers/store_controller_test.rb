require 'test_helper'

class StoreControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_select '#menu a', minimum: 2
    assert_select '#main .entry', 3 # quantidade de registros nos fixtures
    assert_select 'p.title', 'Dos Anjos'
    assert_select 'p.price', /R\$ [.\d]+\,\d\d/
  end

end
