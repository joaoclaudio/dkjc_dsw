require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
    @update = {
      title: 'Lorem Ipsum',
      description: 'Wibbles are fun!',
      image_url: 'lorem.jpg',
      price: 19.95
    }
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)

    assert_select 'tr', 3 # quantidade de linhas da tabela (Quantidade de produtos cadastrados)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: @update
    end

    assert_redirected_to product_path(assigns(:product))

    get :index # voltar para a página products index
    assert_select 'tr', 4 # verificar se a quantidade de produtos aumentou
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success

    assert_select 'h2', 'Dos Anjos' # titulo do produto exibido corretamente
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    patch :update, id: @product, product: @update
    assert_redirected_to product_path(assigns(:product))

    # verificar se houve a alteração
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path

    get :index # voltar para a página products index
    assert_select 'tr', 2 # verificar se a quantidade de produtos decrementou
  end
end
